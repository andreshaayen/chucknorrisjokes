import React from "react";
import ReactDOM from "react-dom";
import GetJokesStyle from "./components/StyledComponents/GetJokes.style";
import GlobalStyle from "./components/StyledComponents/GlobalStyle";

import GetJokes from "./components/GetJokes";

import "./styles/styles.scss";

class App extends React.Component {
  render() {
    return (
      <section>
        <GlobalStyle />
        <GetJokesStyle>
          <GetJokes />
        </GetJokesStyle>
      </section>
    );
  }
}
ReactDOM.render(<App />, document.getElementById("app"));
