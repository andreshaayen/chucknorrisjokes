const Styles = {
  // Colors
  backgroundColor: "#CFE0C3",
  appColor: "#1F363D",
  appColorDarker: "#70A9A1",
  
  appColor2: "#094074",
  accentColor1: "#70c1b3",
  accentColor2: "#b2dbbf",
  accentColor3: "#D3D7ED",
  font: "#F7EDF8",
  font2: '#FBFFF7',
  buttonColor: "#9EC1A3",
  darkBorder: "#7b89ad",
  shadowButtonColor: "#7EA184",
  boxShadow: "5px 3px 3px 0px rgba(0,0,0,0.10)",

  //Spacing
  smallSize: "1.2rem",
  betweenSize: "1.6rem",
  mediumSize: "1.8rem",
  largeSize: "3rem",
  xlSize: "4.8rem"
};

export default Styles;
