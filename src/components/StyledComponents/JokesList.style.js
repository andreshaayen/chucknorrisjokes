import styled from "styled-components";
import Styles from "../stylesObject";

const JokesList = styled.li`
  font-size: ${Styles.betweenSize};
  font-color: ${Styles.font2}
  border-bottom: 1px solid ${Styles.darkBorder};
  width: 70%;
  background-color: ${Styles.appColorDarker};
  text-align: center;
  margin: 0 auto ${Styles.smallSize};
  padding: 7px;
  border-radius: 5px;
  &:hover {
    background: #40798C;
  }
`;

export default JokesList;
