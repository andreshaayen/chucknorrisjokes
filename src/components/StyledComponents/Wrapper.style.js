import styled from "styled-components";
import Styles from "../stylesObject";

const Wrapper = styled.div`
  color: ${Styles.font};
  font-size: ${Styles.largeSize};
  text-align: center;
  width: 100%;
`;
export default Wrapper;
