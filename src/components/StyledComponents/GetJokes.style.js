import styled from "styled-components";
import Styles from "../stylesObject";

const GetJokes = styled.div`
  background: ${Styles.appColor};
  width: 50%;
  display: flow;
  margin: auto;
  margin-top: 12rem;
  border-radius: ${Styles.mediumSize};
`;

export default GetJokes;
