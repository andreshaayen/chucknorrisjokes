import styled, { createGlobalStyle } from "styled-components";
import Styles from "../stylesObject";

const GlobalStyle = createGlobalStyle`
    body {
        background: ${Styles.backgroundColor}
    }
`;

export default GlobalStyle;
