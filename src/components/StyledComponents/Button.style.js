import styled from "styled-components";
import Styles from "../stylesObject";

const Button = styled.button`
  background: ${Styles.buttonColor}
  color: ${Styles.font2}
  font-size: ${Styles.mediumSize};
  cursor: pointer;
  margin: 1em;
  padding: 0.25em 1em;
  border-radius: 5px;
  border-bottom: 2px solid ${Styles.shadowButtonColor};
  box-shadow: ${Styles.boxShadow};
  &:hover {
    background: #7EA184;
  }
`;

export default Button;
// background: ${props => props.primary ? "palevioletred" : "white"};
//   color: ${props => props.primary ? "white" : "palevioletred"};
