import styled from "styled-components";
import Styles from "../stylesObject";

const Heading = styled.h1`
  color: ${Styles.font};
  font-size: ${Styles.largeSize};
  text-align: center;
  width: 100%;
  padding-top: 2rem;
`;
export default Heading;
