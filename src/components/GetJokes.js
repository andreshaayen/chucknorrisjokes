import React, { Component } from "react";
import Button from "./StyledComponents/Button.style";
import jokesService from "../server/jokes.service";
import Favorite from "./Favorite";
import Heading from "./StyledComponents/Header.style";
import Wrapper from "./StyledComponents/Wrapper.style";
import JokesList from "./StyledComponents/JokesList.style";

class GetJokes extends Component {
  state = {
    jokes: [],
    selectedjoke: []
  };

  getJokes = e => {
    e.preventDefault();
    jokesService.getJokes().then(response => {
      console.log(response.data.value);
      const jokes = response.data.value;
      this.setState({
        jokes: jokes
      });
    });
  };

  addJoke = selectedJoke => {
    const selecteJokeArr = this.state.selectedjoke.push(selectedJoke);
    this.setState({
      selectedJoke: selecteJokeArr
    });
  };

  removeJoke = () => {};

  render() {
    const array = this.state.selectedjoke;
    return (
      <Wrapper>
        <Heading>Chuck Norris Jokes</Heading>
        <Button
          className="formButton btn btn-primary"
          onClick={e => this.getJokes(e)}
        >
          Get jokes!
        </Button>
        <ul>
          {this.state.jokes.map(joke => (
            <JokesList key={joke.id} onClick={() => this.addJoke(joke.joke)}>
              {joke.joke}
            </JokesList>
          ))}
        </ul>
          <Favorite listJokes={array} />
      </Wrapper>
    );
  }
}

export default GetJokes;
