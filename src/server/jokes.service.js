import axios from "axios";

const baseUrl = "http://api.icndb.com/jokes/random/10";
const axiosConfig = {
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/json"
  }
};

function getJokes() {
  return axios.get(baseUrl, axiosConfig);
}

let jokesService = { getJokes };

export default jokesService;
